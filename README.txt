-------------------------------------------------------------------------------
* Shakespear Insult Generator: (C) Peter Russo 2012, All Rights Reserved.     *
-------------------------------------------------------------------------------


DISCLAIMER
----------

This software does not claim to be invulnerable to security issues. There may,
in fact, be serious security issues concerning this software. I never said this
software was safe; I don't expect anyone to make the assumption. C and similar
languages are notorious for containing buffer overflows. I do not claim that
they do not exist in this software, although I should hope I've weeded most, if
not all of them, out. Performance is no gaurantee as well. If your system's
performance suffers as a result of using this software, than I sincerely
apologize. Once again, safety is no gauruntee.

In other words, if your computer blows up as a result of using this software,
well, I didn't say it was safe. This applies to all parts of this software, and
I do not claim any responsibility for loss of property, limb, or life as the
result of using this software to the fullest extent permitted by applicable
law.


LICENSE
-------

This software is FREE and OPEN-SOURCE. You are given right to COPY, MODIFY,
USE, and DISTRIBUTE this software, as long as THIS README DOCUMENT is kept
INTACT, IN ITS ENTIRETY. You MAY modify this README, provided that all
modifications are ENTERED UNDER THE DESIGNATED BANNER IN THE ORDER THEY ARE
MADE.

You MAY NOT SELL THIS SOFTWARE OR LICENSES FOR THIS SOFTWARE. You MAY use this
software for profit, e.g., to generate insults for a screenplay, or a script,
or something of that sort. This does NOT give you the right to SELL this
software! This software is to remain in ALL CASES AND IN ALL MODIFICATIONS FREE
AND OPEN-SOURCE.

For all other concerns of the end-user about this software, please refer to the
GNU Public license. All conditions in the GNU Public license (GPL) apply to
this software UNLESS the GPL conflicts with those conditions entered in THIS
DOCUMENT.


MODIFICATIONS
-------------
